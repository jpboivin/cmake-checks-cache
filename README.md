# CMakeChecksCache

**CMakeChecksCache** provides CMake modules that intercept the **platform checks** and globally cache the result for faster generations.

## Introduction

One of the slowest part of CMake is the runtime checks cross platform projects are doing. To determine whether or not an include, a library or a symbol is available, CMake will simply compile a test program and cache the result. When regenerating, it's not too bad, but for cold starts (like on pipelines) it can end up making the generation longer than the compilation itself.

While looking at a way to either cache the checks or the try_compile commands, I found [cmake-checks-cache](https://github.com/cristianadam/cmake-checks-cache). The project was interesting, but wouldn't work for the kind of issues I'm seeing at my job. **cmake-checks-cache** generates a cache file that can be provided to CMake with `-C` on cold starts. This solution has a few drawbacks.

1. The cache is manually generated. If some checks are added, the cache must be manually regenerated.
2. The cache does not take into considerations the environment in which it was generated. If it changes (e.g. compiler version), it might breaks unexpectedly.
3. The cache is generated for one specific environment and platform. Supporting multiple platforms would require a huge effort to maintain all these cache files. For example, at work we support roughly 10 platforms, with some parameters and options impacting the checks we are doing and potentially even the results.
4. The cache does not prevent a project with dependencies to make the checks multiple times (for example, the classical `unistd.h` include check).
5. The cache being specific to a project, it won't speedup the generation of multiple projects. For example, supposing project A depends on B and C, and project B depends on C. If you're working directly on these projects, you might end up doing the `unistd.h` include check of C in A, B and C even if they are generated in the exact same context.

Still, the foundation of the project remains interesting and I decided to pick some of the concepts to build a new platform checks cache, while trying to make it more dynamic and granular.

## Internals

**CMakeChecksCache** will build a global cache containing multiple profiles. A profile is based on the current environment (system, compiler, flags, etc.) and an identifier is derived from it. This profile concept allow for the cache to handle multiple platforms, but also to detect changes more easily.

Once a profile is built, multiple `CheckXXX.cmake` modules will be intercepted and cached. The check will be cached based on the parameters and some variables (like `CMAKE_REQUIRED_FLAGS`) to allow for the cache to be used when the same check is done by sub-projects.

With this design, if the same profile is used by multiple projects on a system, they will all contribute to the same cache.

## Benchmarks

TODO

# Supported Platform Checks

The following platform checks are currently intercepted:
- [CheckIncludeFile](https://cmake.org/cmake/help/latest/module/CheckIncludeFile.html): Provides a macro to check if a header file can be included in C.
- [CheckIncludeFileCXX](https://cmake.org/cmake/help/latest/module/CheckIncludeFileCXX.html): Provides a macro to check if a header file can be included in CXX.
- [CheckIncludeFiles](https://cmake.org/cmake/help/latest/module/CheckIncludeFiles.html): Provides a macro to check if a list of one or more header files can be included together.
- [CheckLibraryExists](https://cmake.org/cmake/help/latest/module/CheckLibraryExists.html): Check if the function exists.
- [CheckSymbolExists](https://cmake.org/cmake/help/latest/module/CheckSymbolExists.html): Provides a macro to check if a symbol exists as a function, variable, or macro in C.
- [CheckCXXSymbolExists](https://cmake.org/cmake/help/latest/module/CheckCXXSymbolExists.html): Provides a macro to check if a symbol exists as a function, variable, or macro in C++.

# F.A.Q.

## 1. Where the global cache is located?

The cache has a different location based on the host system:
- **CMAKE_HOST_WIN32**: `$ENV{LOCALAPPDATA}/CMake/cmake_checks_cache`
- **CMAKE_HOST_APPLE**: `~/Library/Application Support/CMake/cmake_checks_cache`
- **CMAKE_HOST_UNIX**: `~/.cache/cmake/cmake_checks_cache`
- **Others**: `${CMAKE_BINARY_DIR}/cmake_checks_cache`

The cache can be moved by defining `CMAKE_CHECKS_CACHE_BASE_DIR` before enabling the module.

## 2. How can I cleanup the global cache?

The global cache can be cleaned by deleting all subfolders, or by setting `CMAKE_CHECKS_CACHE_CLEAR` to `TRUE` before enabling the module.

## 3. Is it safe to share the cache across multiple machines?

It depends. If for example the machines are running the same image, it will be totally safe (like on a pipeline). If they aren't, it depends on the checks. Checking for `stddef.h` shouldn't break on different machines if the same profile is detected. If some includes and libraries can be made available without an explicit indication to the compiler, and these are optionally installed on a system, sharing a cache might improperly report availability of these. Similarly, when checking for a library, CMake allows a custom location which likely means that the library availability is non-standard. At the end of the day, it really depends on the project. Please review the platform checks before considering sharing the cache across multiple machines.

# License

**CMakeChecksCache** is distributed under the OSI-approved MIT License. See [LICENSE](LICENSE) for more details.