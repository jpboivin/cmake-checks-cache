#
# Copyright (c) 2021 Jean-Philippe Boivin <contact@jpboivin.me>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

cmake_minimum_required(VERSION 3.10)

include_guard(GLOBAL)

# determine the location of the current file...
set(_CCC_BASE_DIR "${CMAKE_CURRENT_LIST_DIR}")

macro(enable_cmake_checks_cache)
    if (NOT CMAKE_CHECKS_CACHE_ENABLED)
        set(CMAKE_CHECKS_CACHE_ENABLED TRUE)

        if (NOT DEFINED CMAKE_CHECKS_CACHE_BASE_DIR)
            if (CMAKE_HOST_WIN32)
                file(TO_CMAKE_PATH "$ENV{LOCALAPPDATA}/CMake/cmake_checks_cache" CMAKE_CHECKS_CACHE_BASE_DIR)
            elseif (CMAKE_HOST_APPLE)
                get_filename_component(CMAKE_CHECKS_CACHE_BASE_DIR "~/Library/Application Support/CMake/cmake_checks_cache" ABSOLUTE)
            elseif (CMAKE_HOST_UNIX)
                get_filename_component(CMAKE_CHECKS_CACHE_BASE_DIR "~/.cache/cmake/cmake_checks_cache" ABSOLUTE)
            else()
                set(CMAKE_CHECKS_CACHE_BASE_DIR "${CMAKE_BINARY_DIR}/cmake_checks_cache")
            endif()
        endif()

        message(STATUS "Using cache for platforms checks")
        if (CMAKE_CHECKS_CACHE_CLEAR)
            message(STATUS "Using cache for platforms checks - cleared")
            file(REMOVE_RECURSE ${CMAKE_CHECKS_CACHE_BASE_DIR})
        endif()

        include("${_CCC_BASE_DIR}/modules/ComputePlatformChecksProfile.cmake")
        list(INSERT CMAKE_MODULE_PATH 0 "${_CCC_BASE_DIR}/modules")
    endif()
endmacro()