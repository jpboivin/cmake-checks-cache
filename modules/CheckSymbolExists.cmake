#
# Copyright (c) 2021 Jean-Philippe Boivin <contact@jpboivin.me>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

cmake_minimum_required(VERSION 3.10)

include_guard(GLOBAL)

include(${CMAKE_ROOT}/Modules/CheckSymbolExists.cmake)

macro(check_symbol_exists symbol files variable)
    if (NOT DEFINED "${variable}")
        # https://cmake.org/cmake/help/latest/module/CheckSymbolExists.html
        cmake_policy(GET CMP0075 _CSE_CMP0075_POLICY_STATE PARENT_SCOPE)
        if (_CSE_CMP0075_POLICY_STATE STREQUAL "NEW" OR _CSE_CMP0075_POLICY_STATE STREQUAL "OLD")
            # forward the state of the policy to the next macro
            cmake_policy(SET CMP0075 "${_CSE_CMP0075_POLICY_STATE}")
        endif()

        set(_CSE_CHECK_PARAMETERS)
        list(APPEND _CSE_CHECK_PARAMETERS "symbol=\"${symbol}\"")
        list(APPEND _CSE_CHECK_PARAMETERS "files=\"${files}\"")

        set(_CSE_CHECK_VARIABLES)
        list(APPEND _CSE_CHECK_VARIABLES "CMAKE_REQUIRED_FLAGS=\"${CMAKE_REQUIRED_FLAGS}\"")
        list(APPEND _CSE_CHECK_VARIABLES "CMAKE_REQUIRED_DEFINITIONS=\"${CMAKE_REQUIRED_DEFINITIONS}\"")
        list(APPEND _CSE_CHECK_VARIABLES "CMAKE_REQUIRED_INCLUDES=\"${CMAKE_REQUIRED_INCLUDES}\"")
        list(APPEND _CSE_CHECK_VARIABLES "CMAKE_REQUIRED_LINK_OPTIONS=\"${CMAKE_REQUIRED_LINK_OPTIONS}\"")
        if (_CSE_CMP0075_POLICY_STATE STREQUAL "NEW")
            list(APPEND _CSE_CHECK_VARIABLES "CMAKE_REQUIRED_LIBRARIES=\"${CMAKE_REQUIRED_LIBRARIES}\"")
        endif()

        string(SHA256 _CSE_CHECK_HASH "check_symbol_exists(${_CSE_CHECK_PARAMETERS}) w/ ${_CSE_CHECK_VARIABLES}")

        set(_CSE_VARIABLE_NAME "_CSE_CACHE_${_CSE_CHECK_HASH}")
        string(TOUPPER ${_CSE_VARIABLE_NAME} _CSE_VARIABLE_NAME)

        set(_CSE_CURRENT_CACHE_FILE "${CMAKE_CHECKS_CACHE_DIR}/CheckSymbolExists.cmake")
        include(${_CSE_CURRENT_CACHE_FILE} OPTIONAL)

        if (NOT DEFINED ${_CSE_VARIABLE_NAME})
            _check_symbol_exists("${symbol}" "${files}" ${_CSE_VARIABLE_NAME})
            file(APPEND ${_CSE_CURRENT_CACHE_FILE} "set(${_CSE_VARIABLE_NAME} \"${${_CSE_VARIABLE_NAME}}\" CACHE INTERNAL \"Have symbol ${symbol}\")\n")
        else()
            if(NOT CMAKE_REQUIRED_QUIET)
                message(CHECK_START "Looking for ${symbol}")
                if (${_CSE_VARIABLE_NAME})
                    message(CHECK_PASS "found (using global cache)")
                else()
                    message(CHECK_FAIL "not found (using global cache)")
                endif()
            endif()
        endif()

        set(${variable} "${${_CSE_VARIABLE_NAME}}" CACHE INTERNAL "Have symbol ${symbol}")
    endif()
endmacro()