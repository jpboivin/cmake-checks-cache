#
# Copyright (c) 2021 Jean-Philippe Boivin <contact@jpboivin.me>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

cmake_minimum_required(VERSION 3.10)

include_guard(GLOBAL)

include(${CMAKE_ROOT}/Modules/CheckLibraryExists.cmake)

macro(check_library_exists library function location variable)
    if (NOT DEFINED "${variable}")
        # https://cmake.org/cmake/help/latest/module/CheckLibraryExists.html
        set(_CLE_CHECK_PARAMETERS)
        list(APPEND _CLE_CHECK_PARAMETERS "library=\"${library}\"")
        list(APPEND _CLE_CHECK_PARAMETERS "function=\"${function}\"")
        list(APPEND _CLE_CHECK_PARAMETERS "location=\"${location}\"")

        set(_CLE_CHECK_VARIABLES)
        list(APPEND _CLE_CHECK_VARIABLES "CMAKE_REQUIRED_FLAGS=\"${CMAKE_REQUIRED_FLAGS}\"")
        list(APPEND _CLE_CHECK_VARIABLES "CMAKE_REQUIRED_DEFINITIONS=\"${CMAKE_REQUIRED_DEFINITIONS}\"")
        list(APPEND _CLE_CHECK_VARIABLES "CMAKE_REQUIRED_LINK_OPTIONS=\"${CMAKE_REQUIRED_LINK_OPTIONS}\"")
        list(APPEND _CLE_CHECK_VARIABLES "CMAKE_REQUIRED_LIBRARIES=\"${CMAKE_REQUIRED_LIBRARIES}\"")

        string(SHA256 _CLE_CHECK_HASH "check_library_exists(${_CLE_CHECK_PARAMETERS}) w/ ${_CLE_CHECK_VARIABLES}")

        set(_CLE_VARIABLE_NAME "_CLE_CACHE_${_CLE_CHECK_HASH}")
        string(TOUPPER ${_CLE_VARIABLE_NAME} _CLE_VARIABLE_NAME)

        set(_CLE_CURRENT_CACHE_FILE "${CMAKE_CHECKS_CACHE_DIR}/CheckLibraryExists.cmake")
        include(${_CLE_CURRENT_CACHE_FILE} OPTIONAL)

        if (NOT DEFINED ${_CLE_VARIABLE_NAME})
            _check_library_exists("${library}" "${function}" "${location}" ${_CLE_VARIABLE_NAME})
            file(APPEND ${_CLE_CURRENT_CACHE_FILE} "set(${_CLE_VARIABLE_NAME} \"${${_CLE_VARIABLE_NAME}}\" CACHE INTERNAL \"Have library ${library}\")\n")
        else()
            if(NOT CMAKE_REQUIRED_QUIET)
                message(CHECK_START "Looking for ${function} in ${library}")
                if (${_CLE_VARIABLE_NAME})
                    message(CHECK_PASS "found (using global cache)")
                else()
                    message(CHECK_FAIL "not found (using global cache)")
                endif()
            endif()
        endif()

        set(${variable} "${${_CLE_VARIABLE_NAME}}" CACHE INTERNAL "Have library ${library}")
    endif()
endmacro()