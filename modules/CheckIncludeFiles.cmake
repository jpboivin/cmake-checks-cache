#
# Copyright (c) 2021 Jean-Philippe Boivin <contact@jpboivin.me>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

cmake_minimum_required(VERSION 3.10)

include_guard(GLOBAL)

include(${CMAKE_ROOT}/Modules/CheckIncludeFiles.cmake)

macro(check_include_files includes variable)
    # from the original module itself
    if("x${ARGN}" STREQUAL "x")
        if(CMAKE_C_COMPILER_LOADED)
            set(language C)
        elseif(CMAKE_CXX_COMPILER_LOADED)
            set(language CXX)
        else()
            message(FATAL_ERROR "CHECK_INCLUDE_FILES needs either C or CXX language enabled.\n")
        endif()
    elseif("x${ARGN}" MATCHES "^xLANGUAGE;([a-zA-Z]+)$")
        set(language "${CMAKE_MATCH_1}")
    elseif("x${ARGN}" MATCHES "^xLANGUAGE$")
        message(FATAL_ERROR "No languages listed for LANGUAGE option.\nSupported languages: C, CXX.\n")
    else()
        message(FATAL_ERROR "Unknown arguments:\n  ${ARGN}\n")
    endif()

    if (NOT DEFINED "${variable}")
        # https://cmake.org/cmake/help/latest/module/CheckIncludeFiles.html
        cmake_policy(GET CMP0075 _CIF_CMP0075_POLICY_STATE PARENT_SCOPE)
        if (_CIF_CMP0075_POLICY_STATE STREQUAL "NEW" OR _CIF_CMP0075_POLICY_STATE STREQUAL "OLD")
            # forward the state of the policy to the next macro
            cmake_policy(SET CMP0075 "${_CIF_CMP0075_POLICY_STATE}")
        endif()

        set(_CIF_CHECK_PARAMETERS)
        list(APPEND _CIF_CHECK_PARAMETERS "includes=\"${includes}\"")
        list(APPEND _CIF_CHECK_PARAMETERS "language=\"${language}\"")

        set(_CIF_CHECK_VARIABLES)
        list(APPEND _CIF_CHECK_VARIABLES "CMAKE_REQUIRED_FLAGS=\"${CMAKE_REQUIRED_FLAGS}\"")
        list(APPEND _CIF_CHECK_VARIABLES "CMAKE_REQUIRED_DEFINITIONS=\"${CMAKE_REQUIRED_DEFINITIONS}\"")
        list(APPEND _CIF_CHECK_VARIABLES "CMAKE_REQUIRED_INCLUDES=\"${CMAKE_REQUIRED_INCLUDES}\"")
        list(APPEND _CIF_CHECK_VARIABLES "CMAKE_REQUIRED_LINK_OPTIONS=\"${CMAKE_REQUIRED_LINK_OPTIONS}\"")
        if (_CIF_CMP0075_POLICY_STATE STREQUAL "NEW")
            list(APPEND _CIF_CHECK_VARIABLES "CMAKE_REQUIRED_LIBRARIES=\"${CMAKE_REQUIRED_LIBRARIES}\"")
        endif()

        string(SHA256 _CIF_CHECK_HASH "check_include_files(${_CIF_CHECK_PARAMETERS}) w/ ${_CIF_CHECK_VARIABLES}")

        set(_CIF_VARIABLE_NAME "_CIF_CACHE_${_CIF_CHECK_HASH}")
        string(TOUPPER ${_CIF_VARIABLE_NAME} _CIF_VARIABLE_NAME)

        set(_CIF_CURRENT_CACHE_FILE "${CMAKE_CHECKS_CACHE_DIR}/CheckIncludeFiles.cmake")
        include(${_CIF_CURRENT_CACHE_FILE} OPTIONAL)

        if (NOT DEFINED ${_CIF_VARIABLE_NAME})
            _check_include_files("${includes}" ${_CIF_VARIABLE_NAME} ${ARGN})
            file(APPEND ${_CIF_CURRENT_CACHE_FILE} "set(${_CIF_VARIABLE_NAME} \"${${_CIF_VARIABLE_NAME}}\" CACHE INTERNAL \"Have include ${includes}\")\n")
        else()
            if(NOT CMAKE_REQUIRED_QUIET)
                set(_CIF_INCLUDES ${includes}) # remove empty elements
                if("${_CIF_INCLUDES}" MATCHES "^([^;]+);.+;([^;]+)$")
                    list(LENGTH _CIF_INCLUDES _CIF_INCLUDES_LEN)
                    set(_description "${_CIF_INCLUDES_LEN} include files ${CMAKE_MATCH_1}, ..., ${CMAKE_MATCH_2}")
                elseif("${_CIF_INCLUDES}" MATCHES "^([^;]+);([^;]+)$")
                    set(_description "include files ${CMAKE_MATCH_1}, ${CMAKE_MATCH_2}")
                else()
                    set(_description "include file ${_CIF_INCLUDES}")
                endif()

                message(CHECK_START "Looking for ${_description}")
                if (${_CIF_VARIABLE_NAME})
                    message(CHECK_PASS "found (using global cache)")
                else()
                    message(CHECK_FAIL "not found (using global cache)")
                endif()
            endif()
        endif()

        set(${variable} "${${_CIF_VARIABLE_NAME}}" CACHE INTERNAL "Have include ${includes}")
    endif()
endmacro()